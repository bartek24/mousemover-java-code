import java.awt.Robot;
import java.util.Random;
class SystemNolock {
    public static final int TWENTY_SEC = 20000; //Time of delay, you can choose as per convenience.
    public static final int POSITION_Y = 100;
    public static final int POSITION_X = 100;
    public static void main(String... args) throws Exception {
    Robot robot = new Robot();
    Random random = new Random();
    while (true) {
        robot.mouseMove(random.nextInt(POSITION_X), random.nextInt(POSITION_Y));
        Thread.sleep(TWENTY_SEC);
        }
    }
}
